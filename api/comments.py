from flask import request, abort

from services.comments_provider import CommentProvider

comments = CommentProvider()


def create():
    user_uid = request.headers.get('user-uid')
    if not user_uid:
        abort(400, {'error': 'Request needs user-uid header'})

    body = request.json
    comment = comments.create(body, user_uid)
    return comments.to_dict(comment)


def post_comments(post_id):
    return [comments.to_dict(comment) for comment in comments.by_post(post_id)]
