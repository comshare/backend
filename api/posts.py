from flask import request, abort

from services.posts_provider import PostProvider

posts = PostProvider()


def get(post_id):
    return posts.get(post_id)


def search() -> list:
    offset = request.args.get('offset')
    limit = request.args.get('limit')

    return [posts.to_dict(post) for post in posts.list(offset=offset, limit=limit)]


def create() -> dict:
    user_uid = request.headers.get('user-uid')
    if not user_uid:
        abort(400, {'error': 'Request needs user-uid header'})

    body = request.json
    post = posts.create(body, user_uid=user_uid)
    return posts.to_dict(post)


def get_feed():
    offset = request.args.get('offset')
    limit = request.args.get('limit')
    lat = request.args.get('lat')
    lon = request.args.get('lon')

    return [posts.to_dict(post) for post in posts.get_by_distance(float(lon), float(lat), offset=offset, limit=limit)]


def user_posts():
    user_uid = request.headers.get('user-uid')
    if not user_uid:
        abort(400, {'error': 'Request needs user-uid header'})

    return [posts.to_dict(post) for post in posts.get_by_user_uid(user_uid)]
