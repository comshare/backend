from flask import request, abort

from services import LocationProvider

locations = LocationProvider()


def search() -> list:
    offset = request.args.get('offset')
    limit = request.args.get('limit')

    return [locations.to_dict(location) for location in locations.list_by_newest(offset=offset, limit=limit)]


def create() -> dict:
    user_uid = request.headers.get('user-uid')
    if not user_uid:
        abort(400, {'error': 'Request needs user-uid header'})

    body = request.json
    location = locations.create(body, user_uid=user_uid)
    return locations.to_dict(location)
