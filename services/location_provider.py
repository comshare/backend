from typing import List, Optional

from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from sqlalchemy.orm import Query

from models import Location
from postgis import GeographySerializationField
from services.data_provider import SQLAlchemyProvider


class LocationSchema(SQLAlchemySchema):
    class Meta:
        model = Location
        load_instance = True

    id = auto_field(dump_only=True)
    created = auto_field(dump_only=True)
    userUid = auto_field(dump_only=True)

    location = GeographySerializationField()


class LocationProvider(SQLAlchemyProvider):
    model = Location
    schema = LocationSchema()

    def create(self, body, user_uid):
        item = self.from_dict(body)
        item.userUid = user_uid
        self.save(item)
        return item

    def list_by_newest(self, query: Optional[Query] = None, offset: int = None, limit: int = None) -> List[Location]:
        q = query or self.model.query
        q = q.order_by(self.model.created.desc())
        return self.list(q, offset, limit)

