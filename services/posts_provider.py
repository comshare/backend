from hashlib import md5

from geoalchemy2.shape import from_shape
from marshmallow import fields
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from shapely.geometry import Point
from sqlalchemy import func

from models import Post
from postgis import GeographySerializationField
from services.data_provider import SQLAlchemyProvider


class PostSchema(SQLAlchemySchema):
    class Meta:
        model = Post
        load_instance = True

    id = auto_field(dump_only=True)
    title = auto_field(required=True)
    content = auto_field(required=True)
    created = auto_field(dump_only=True)

    location = GeographySerializationField()

    user_handle = fields.Function(lambda obj: md5(f"{obj.authorUid}-{obj.id}".encode('utf-8')).hexdigest())


class PostProvider(SQLAlchemyProvider):
    model = Post
    schema = PostSchema()

    def to_dict(self, obj):
        d = super().to_dict(obj)
        return {**d, 'comments_count': obj.comments_count}

    def get_by_distance(self, lat, lon, **kwargs):
        distance = Post.get_distance_func(lat, lon)

        wkb_element = from_shape(Point(lon, lat))

        distance_filter = func.ST_DFullyWithin(Post.location, wkb_element, 1162.1371 * 0.014472)

        q = self.session.query(Post).order_by(distance)

        return self.list(query=q, **kwargs)

    def get_by_user_uid(self, user_uid, **kwargs):
        q = Post.query.filter(Post.authorUid == user_uid).order_by(Post.created.desc())
        return self.list(query=q, **kwargs)

    def create(self, body, user_uid):
        item = self.from_dict(body)
        item.authorUid = user_uid
        self.save(item)
        return item
