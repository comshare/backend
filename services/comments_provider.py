from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

from models import Comment, Post
from services.data_provider import SQLAlchemyProvider


class CommentSchema(SQLAlchemySchema):
    class Meta:
        model = Comment
        load_instance = True

    id = auto_field(dump_only=True)
    created = auto_field(dump_only=True)

    content = auto_field(required=True)
    post_id = auto_field(required=True)

    user_handle = auto_field(dump_only=True)


class CommentProvider(SQLAlchemyProvider):
    model = Comment
    schema = CommentSchema()

    def create(self, body, user_uid):
        item: Comment = super().create(body)
        item.user_handle = Comment.get_user_handle(item.post_id, user_uid)
        self.save()
        return item

    def by_post(self, post_id):
        post = Post.query.filter(Post.id == post_id).first()

        return post.comments if post else None
