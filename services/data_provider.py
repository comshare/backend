from typing import List, Optional, Union

from marshmallow import Schema
from sqlalchemy.orm import Session, Query

from db import db


class SQLAlchemyProvider:
    model: db.Model = None
    schema: Schema = None

    default_offset = 0
    default_limit = 10

    db = db
    session: Session = db.session

    def __init__(self):
        assert self.model

    def save(self, to_save: Optional[Union[List[db.Model], db.Model]] = None):
        if type(to_save) is list:
            self.session.add_all(to_save)
        elif issubclass(type(to_save), db.Model):
            self.session.add(to_save)

        self._commit_or_rollback()

    def _commit_or_rollback(self):
        try:
            self.session.commit()
        except Exception as e:
            self.session.rollback()
            raise e

    def pre_save(self, item): pass
    def post_save(self, item): pass

    def to_dict(self, obj: db.Model) -> dict:
        return self.schema.dump(obj) if self.schema else obj.__dict__

    def from_dict(self, body: dict) -> db.Model:
        return self.schema.load(body, session=self.session) if self.schema else self.model(**body)

    def get(self, item_id: int) -> db.Model:
        q = self.model.query.filter(self.model.id == item_id)
        return q.first()

    def list(self, query: Optional[Query] = None, offset: int = default_offset, limit: int = default_limit) -> List[db.Model]:
        q = query or self.model.query
        q = q.offset(offset or self.default_offset).limit(limit or self.default_limit)
        return q.all()

    def create(self, body, **kwargs):
        item = self.from_dict(body)
        self.pre_save(item)
        self.save(item)
        self.post_save(item)
        return item
