import geoalchemy2
from geoalchemy2 import Geometry
from geoalchemy2.shape import from_shape
from shapely.geometry import Point
from sqlalchemy import func, select
from sqlalchemy.orm import column_property
from sqlalchemy_utc import utcnow

from db import db
from models import Comment


class Post(db.Model):
    __tablename__ = "post"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.UtcDateTime, default=utcnow())
    modified = db.Column(db.UtcDateTime, server_onupdate=utcnow())

    title = db.Column(db.String(255))
    content = db.Column(db.Text)

    authorUid = db.Column(db.String(255), index=True)

    location = db.Column(Geometry('POINT', srid=4326))

    comments_count = column_property(
        select([func.count(Comment.id)]).where(Comment.post_id == id).correlate_except(Comment)
    )

    @classmethod
    def get_distance_func(cls, lon, lat):
        point = from_shape(Point(lon, lat), srid=4326)

        return cls.location.ST_Distance(point).label('distance')
