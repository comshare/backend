from hashlib import md5

from sqlalchemy_utc import utcnow

from db import db


class Comment(db.Model):
    __tablename__ = "comment"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.UtcDateTime, default=utcnow())
    modified = db.Column(db.UtcDateTime, server_onupdate=utcnow())

    post_id = db.Column(db.Integer, db.ForeignKey('post.id'), index=True)
    post = db.relationship('Post', backref=db.backref('comments', lazy=True))

    content = db.Column(db.Text)
    user_handle = db.Column(db.String)

    @staticmethod
    def get_user_handle(post_id, user_uid):
        return md5(f"{user_uid}-{post_id}".encode('utf-8')).hexdigest()
