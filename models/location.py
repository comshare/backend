from geoalchemy2 import Geometry
from sqlalchemy_utc import utcnow

from db import db


class Location(db.Model):
    __tablename__ = "location"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.UtcDateTime, default=utcnow())

    userUid = db.Column(db.String(255), index=True)
    location = db.Column(Geometry('POINT', srid=4326))
