from flask_injector import request, FlaskInjector
from injector import Binder, singleton

from app import flask_app
from services import PostProvider


def data_providers(binder: Binder):
    binder.bind(
        PostProvider,
        to=PostProvider(),
        scope=singleton,
    )


FlaskInjector(app=flask_app, modules=[
    data_providers,
])
