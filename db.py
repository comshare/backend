from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utc import UtcDateTime

db = SQLAlchemy()
db.UtcDateTime = UtcDateTime
