from geoalchemy2 import Geography, WKBElement, WKTElement
from marshmallow import fields
from marshmallow_sqlalchemy import ModelConverter
from sqlalchemy import func

from db import db


class GeoConverter(ModelConverter):
    SQLA_TYPE_MAPPING = ModelConverter.SQLA_TYPE_MAPPING.copy()
    SQLA_TYPE_MAPPING.update({
        Geography: fields.String
    })


class GeographySerializationField(fields.String):
    def _serialize(self, value, attr, obj, **kwargs):
        if attr == 'location':
            return {
                'latitude': db.session.scalar(func.ST_X(value)),
                'longitude': db.session.scalar(func.ST_Y(value)),
            }

        return None

    def _deserialize(self, value, attr, data, **kwargs):
        if attr == 'location':
            lon = str(value.get('longitude'))
            lat = str(value.get('latitude'))
            return WKTElement(f"POINT({lon} {lat})", srid=4326)

        return None
