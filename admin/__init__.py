from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from db import db
from models import Post, Comment, Location

views = [
    ModelView(Post, db.session),
    ModelView(Comment, db.session),
    ModelView(Location, db.session),
]

admin = Admin(name='microblog', template_mode='bootstrap3')
admin.add_views(*views)
