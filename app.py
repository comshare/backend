from connexion.resolver import RestyResolver
import connexion
from flask_cors import CORS
from flask_migrate import Migrate

from admin import admin
from models import *

from config import DatabaseConfig
from db import db

app = connexion.App(__name__, specification_dir='swagger/')
options = {'swagger_url': '/'}
api = app.add_api('docs.yaml', resolver=RestyResolver('api'), options=options)

flask_app = app.app
flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
flask_app.config = {
    **flask_app.config,
    **DatabaseConfig.__dict__,
    'FLASK_ADMIN_SWATCH': 'cerulean',
}
flask_app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
db.init_app(flask_app)
admin.init_app(flask_app)

migrate = Migrate(flask_app, db)
CORS(flask_app)

if __name__ == '__main__':
    app.run()
