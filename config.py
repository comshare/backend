import os

env = os.environ


class DatabaseConfig:
    database = env.get('DB_DATABASE', 'sqlite')
    user = env.get('DB_USER', '')
    password = env.get('DB_PASSWORD', '')
    domain = env.get('DB_DOMAIN', '')
    path = env.get('DB_PATH', 'tmp.db')

    SQLALCHEMY_DATABASE_URI = env.get('DATABASE_URL') or f'{database}://{user}:{password}@{domain}/{path}'
